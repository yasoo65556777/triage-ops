# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage/event'
require_relative '../../../triage/processor/gitlab_internal_commands/command_retry_pipeline_or_job'

RSpec.describe Triage::CommandRetryPipelineOrJob do
  include_context 'with event', Triage::IssueNoteEvent do
    let(:event_attrs) do
      {
        project_id: project_id,
        new_comment: command_comment,
        event_actor_id: triager_id,
        label_names: label_names,
        by_team_member?: by_team_member,
        payload: {
          'object_attributes' => { 'discussion_id' => discussion_id },
          'project' => { 'path_with_namespace' => project_path_with_namespace }
        }
      }
    end

    let(:command_comment)              { '@gitlab-bot retry_job 123' }
    let(:triager_id)                   { 1 }
    let(:label_names)                  { ['master:broken'] }
    let(:by_team_member)               { true }
    let(:discussion_id)                { 1 }
    let(:project_id)                   { Triage::Event::MASTER_BROKEN_INCIDENT_PROJECT_ID }
    let(:project_path_with_namespace)  { 'gitlab-org' }
    let(:distribution_members)         { [double('User', id: 1), double('User', id: 2)] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.note']

  describe '#applicable?' do
    before do
      allow(Triage.api_client).to receive(:group_members).with(Triage::GITLAB_ORG_DISTRIBUTION_GROUP).and_return(distribution_members)
    end

    it_behaves_like 'applicable on contextual event'

    context 'when command is retry_pipeline' do
      let(:command_comment) { '@gitlab-bot retry_pipeline 123' }

      include_examples 'event is applicable'
    end

    context 'when pipeline label contains master-foss:broken' do
      let(:label_names) { ['master:foss-broken'] }

      include_examples 'event is applicable'
    end

    context 'when actor is not a team member' do
      let(:by_team_member) { false }

      include_examples 'event is not applicable'
    end

    context 'when resource issue is from omnibus-gitlab project' do
      let(:project_id) { Triage::Event::OMNIBUS_PROJECT_ID }

      include_examples 'event is applicable'
    end

    context 'when resource issue is not from master broken incident or omnibus-gitlab project' do
      let(:project_id) { 123 }

      context 'when the project namespace is gitlab-org' do
        include_examples 'event is not applicable'
      end

      context 'when the project namespace is gitlab-org/distribution' do
        let(:project_path_with_namespace) { 'gitlab-org/distribution/project' }

        context 'when event user is not a distribution member' do
          let(:triager_id) { 33333 }

          include_examples 'event is not applicable'
        end

        context 'when event user is a distribution member' do
          include_examples 'event is applicable'
        end
      end

      context 'when the project namespace is gitlab-org/charts' do
        let(:project_path_with_namespace) { 'gitlab-org/charts/project' }

        include_examples 'event is applicable'
      end

      context 'when the project namespace is gitlab-org/build' do
        let(:project_path_with_namespace) { 'gitlab-org/build/project' }

        include_examples 'event is applicable'
      end

      context 'when the project namespace is gitlab-org/cloud-native' do
        let(:project_path_with_namespace) { 'gitlab-org/cloud-native/project' }

        include_examples 'event is applicable'
      end
    end

    context 'when command name is not valid' do
      let(:command_comment) { '@gitlab-bot retry_invalid 234' }

      include_examples 'event is not applicable'
    end

    context 'when issue label does not indicate pipeline project' do
      let(:label_names) { ['backend'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples 'retry pipeline or job' do |resource_type|
      let(:args_id)                 { '123' }
      let(:web_url)                 { 'web_url' }
      let(:response_request_double) { double('request', base_uri: '', path: '') }
      let(:client_request)          { resource_type == 'pipeline' ? :retry_pipeline : :job_retry }

      before do
        event_attrs[:new_comment] = "@gitlab-bot retry_#{resource_type} #{args_id}"
        allow(Triage.api_client).to receive(client_request).and_return({ 'web_url' => web_url })
      end

      it_behaves_like 'rate limited discussion notes requests', count: 100, period: 3600

      context 'with incident labeled with master:broken' do
        let(:label_names) { ['master:broken'] }

        it "replies with retried #{resource_type} url" do
          expect_discussion_notes_request(
            event: event,
            body: "Retried #{resource_type} at #{web_url}."
          ) do
            subject.process
          end
        end
      end

      context 'with incident labeled with master:foss-broken' do
        let(:label_names) { ['master:foss-broken'] }

        it "replies with retried #{resource_type} url" do
          expect_discussion_notes_request(
            event: event,
            body: "Retried #{resource_type} at #{web_url}."
          ) do
            subject.process
          end
        end
      end

      context 'with incident not labeled with master:broken or master:foss-broken' do
        let(:label_names) { ['master:unkown-project-broken'] }

        it 'does nothing' do
          expect_no_request { subject.process }
        end
      end

      context 'when request returned error' do
        let(:error_response_double) do
          double('response',
            code: error_code,
            request: response_request_double,
            parsed_response: { message: 'error' })
        end

        before do
          allow(Triage.api_client).to receive(client_request).and_raise(error)
        end

        context 'with Gitlab::Error::NotFound error' do
          let(:error_code) { '404' }
          let(:error) { Gitlab::Error::NotFound.new(error_response_double) }

          it "replies with invalid #{resource_type} id" do
            expect_discussion_notes_request(
              event: event,
              body: "Command failed, #{args_id} is not a valid #{resource_type} id."
            ) do
              subject.process
            end
          end
        end

        context 'with Gitlab::Error::Forbidden' do
          let(:error_code) { '403' }
          let(:error) { Gitlab::Error::Forbidden.new(error_response_double) }

          it 'replies with forbidden retry response' do
            expect_discussion_notes_request(
              event: event,
              body: "Command failed, #{resource_type} #{args_id} cannot be retried at this time. See [troubleshooting guidelines](https://handbook.gitlab.com/handbook/engineering/workflow/#pro-tips-for-triage-dri) for details."
            ) do
              subject.process
            end
          end
        end

        context 'with other error types' do
          let(:error_code) { '500' }
          let(:error) { Gitlab::Error::InternalServerError.new(error_response_double) }

          it 'replies with general failure response' do
            expect_discussion_notes_request(
              event: event,
              body: 'Command failed! Try again later.'
            ) do
              subject.process
            end
          end
        end
      end
    end

    context 'when retrying pipeline' do
      it_behaves_like 'retry pipeline or job', 'pipeline'
    end

    context 'when retrying job' do
      it_behaves_like 'retry pipeline or job', 'job'
    end
  end
end
