# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/engineering_productivity/pipeline_failure_management'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::PipelineFailureManagement do
  include_context 'with event', Triage::PipelineEvent do
    let(:event_attrs) do
      {
        id: '42',
        status: event_status,
        web_url: 'pipeline_web_url'
      }
    end

    let(:event_status) { 'failed' }
  end

  let(:slack_webhook_url) { 'slack_webhook_url' }
  let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }
  let(:api_client_double)  { double('API client') }
  let(:matching_incidents) { [] }
  let(:search_params) { { in: 'DESCRIPTION', search: event.id, order_by: "created_at", per_page: 2, sort: "desc" } }

  before do
    stub_env('SLACK_WEBHOOK_URL', slack_webhook_url)
    allow(Triage::PipelineFailure::ConfigSelector)
        .to receive(:find_config).with(event, described_class::CONFIGURATION_CLASSES)
        .and_return(config)

    allow(Triage).to receive(:api_client).and_return(api_client_double)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['pipeline.failed', 'pipeline.success']

  describe '#applicable?' do
    include_examples 'applicable on contextual event'

    context 'when no config is found' do
      let(:config) { nil }

      include_examples 'event is not applicable'
    end

    context 'with pipeline.success event' do
      let(:event_status) { 'success' }

      before do
        allow(api_client_double).to receive(:issues).with(
          config.incident_project_id, search_params
        ).and_return(matching_incidents)
      end

      context 'when no previouly reported incident found for the pipeline' do
        include_examples 'event is not applicable'
      end

      context 'when there was a previously reported incident for the pipeline' do
        let(:matching_incidents) { [double('Incident')] }

        include_examples 'event is applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:failed_jobs)      { instance_double(Triage::PipelineFailure::FailedJobs) }
    let(:created_incident) { double('Incident', state: 'opened') }
    let(:incident_creator) { instance_double(Triage::PipelineFailure::IncidentCreator, execute: created_incident) }
    let(:slack_notifier)   { instance_double(Triage::PipelineFailure::SlackNotifier, execute: true) }

    before do
      allow(Triage::PipelineFailure::FailedJobs)
        .to receive(:new).with(event).and_return(failed_jobs)
      allow(failed_jobs).to receive(:execute).and_return([])

      allow(Triage::PipelineFailure::IncidentCreator)
        .to receive(:new)
        .and_return(incident_creator)
      allow(Triage::PipelineFailure::SlackNotifier)
        .to receive(:new)
        .and_return(slack_notifier)

      allow(api_client_double).to receive(:issues).with(
        config.incident_project_id, search_params
      ).and_return(matching_incidents)
    end

    shared_examples 'Slack posting' do |create_incident: true|
      it 'posts to Slack' do
        notify_with_incident = create_incident ? created_incident : nil

        expect(Triage::PipelineFailure::SlackNotifier)
          .to receive(:new)
          .with(
            event: event,
            config: config,
            failed_jobs: [],
            slack_webhook_url: slack_webhook_url,
            incident: notify_with_incident
          ).and_return(slack_notifier)
        expect(slack_notifier).to receive(:execute).and_return(created_incident)

        subject.process
      end
    end

    context 'when config.create_incident? == true' do
      before do
        allow(config).to receive(:create_incident?).and_return(true)
      end

      it 'creates an incident' do
        expect(Triage::PipelineFailure::IncidentCreator)
          .to receive(:new).with(event: event, config: config, failed_jobs: [])
          .and_return(incident_creator)
        expect(incident_creator).to receive(:execute).and_return(created_incident)

        subject.process
      end

      it_behaves_like 'Slack posting'
    end

    context 'when config.create_incident? == false' do
      let(:incident) { nil }

      before do
        allow(config).to receive(:create_incident?).and_return(false)
      end

      it 'does not create an incident' do
        expect(incident_creator).not_to receive(:execute)

        subject.process
      end

      context 'when GitLab API returns 500' do
        let(:incident_finder) { instance_double(Triage::PipelineFailure::PipelineIncidentFinder) }

        before do
          allow(subject).to receive(:incident_finder).and_return(incident_finder)
          allow(incident_finder).to receive(:latest_incident).and_raise('Broken API!')
        end

        # regression test for https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/1364
        it 'does not search existing incidents' do
          expect(incident_creator).not_to receive(:execute)

          expect { subject.process }.not_to raise_error
        end
      end

      it_behaves_like 'Slack posting', create_incident: false
    end

    context 'when created incident state is closed' do
      let(:created_incident) { double('Incident', state: 'closed') }

      it 'does not post to Slack' do
        expect(Triage::PipelineFailure::SlackNotifier).not_to receive(:new)

        subject.process
      end
    end

    context 'when a previous incident was reported with the same pipeline id' do
      let(:latest_incident_state) { 'closed' }
      let(:matching_incidents) do
        [double('Incident',
          iid: '1', 'description' => '[42](pipeline_url)',
          project_id: config.incident_project_id,
          'title' => 'incident',
          'state' => latest_incident_state)]
      end

      context 'with successful event' do
        let(:event_status) { 'success' }

        it 'updates the incident with retry status, does not modify or create incident, no Slack posting' do
          expect(api_client_double).to receive(:post)
            .with(
              "/projects/#{config.incident_project_id}/issues/1/discussions",
              body: { body: '[#42](pipeline_web_url) retry status: success :white_check_mark:.' })

          expect(api_client_double).not_to receive(:reopen_issue)
          expect(api_client_double).not_to receive(:create_issue)
          expect(Triage::PipelineFailure::SlackNotifier).not_to receive(:new)

          subject.process
        end
      end

      context 'with retried and failed pipeline' do
        context 'when latest incident was closed' do
          let(:latest_incident_state) { 'closed' }

          it 'reopens the incident and posts retry status, does not create a new incident, posts to Slack' do
            expect(api_client_double).to receive(:post)
              .with(
                "/projects/#{config.incident_project_id}/issues/1/discussions",
                body: { body: '[#42](pipeline_web_url) retry status: failed :x:.' })

            expect(api_client_double).to receive(:reopen_issue)
            expect(api_client_double).not_to receive(:create_issue)
            expect(Triage::PipelineFailure::SlackNotifier).to receive(:new)

            subject.process
          end
        end

        context 'when latest incident is still open' do
          let(:latest_incident_state) { 'opened' }

          it 'posts retry status, does not reopen or create incident, no Slack posting' do
            expect(api_client_double).to receive(:post)
              .with(
                "/projects/#{config.incident_project_id}/issues/1/discussions",
                body: { body: '[#42](pipeline_web_url) retry status: failed :x:.' })

            expect(api_client_double).not_to receive(:reopen_issue)
            expect(api_client_double).not_to receive(:create_issue)
            expect(Triage::PipelineFailure::SlackNotifier).not_to receive(:new)

            subject.process
          end
        end
      end
    end
  end
end
