# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/generate/ci_job'

RSpec.describe Generate::CiJob do
  let(:options) { double(:options, template: template_path) }
  let(:template_path) { 'path/to/template' }
  let(:template_name) { File.basename(template_path) }
  let(:output_dir) { described_class.new(template: template_path).__send__(:destination) }

  describe '.run' do
    shared_examples 'generates and writes CI file' do |team_name|
      it 'generates and writes CI file' do
        expect(File)
        .to receive(:read)
        .with(template_path)
        .and_return(<<~ERB)
          <% teams.each do |team| -%>
          <%= team.key %>: <%= team.policy_file %>
          <% end -%>
        ERB

        expect(FileUtils)
          .to receive(:mkdir_p)
          .with(output_dir)

        expect(Dir).to receive(:[]).and_return(["#{team_name}.yml"])

        expect(File)
          .to receive(:write)
          .with(
            "#{output_dir}/#{template_name}.yml",
            <<~MARKDOWN)
              #{team_name}: policies/generated/#{template_name}/#{team_name}.yml
            MARKDOWN

        described_class.run(options)
      end
    end

    context 'when the policy is for a group' do
      it_behaves_like 'generates and writes CI file', 'group1'
    end

    context 'when the policy is for a stage' do
      it_behaves_like 'generates and writes CI file', 'stage_with_two_groups'
    end

    context 'when the policy is for an unknown entity' do
      it 'raises an error' do
        expect(Dir).to receive(:[]).and_return(["unknown.yml"])

        expect { described_class.run(options) }.to raise_error("`unknown` is not a valid key for a group nor a stage!")
      end
    end
  end

  describe described_class::PeopleSetForCiJob do
    let(:group) { described_class.new(Hierarchy::Group.new('group1')) }

    describe '#policy_file' do
      it 'sets #policy_file and allow to read it' do
        group.policy_file = 'policy_file'

        expect(group.policy_file).to eq('policy_file')
      end
    end
  end
end
