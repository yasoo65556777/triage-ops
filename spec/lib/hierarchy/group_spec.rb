# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/hierarchy/group'

RSpec.describe Hierarchy::Group do
  subject(:group) { described_class.new('group1') }

  describe '.raw_data' do
    it 'returns WwwGitLabCom.groups data' do
      expect(described_class.raw_data).to include('group1' => hash_including('name' => 'Group 1'))
    end
  end

  describe '#product_managers' do
    it 'returns product managers for team' do
      expect(group.product_managers).to eq(['@fake_pm'])
    end
  end

  describe '#engineering_managers' do
    context 'when scope is all' do
      it 'returns all engineering managers for team' do
        expect(group.engineering_managers(:all)).to eq(%w[@fake_be_em @fake_em @fake_fe_em @fake_fs_em])
      end
    end

    context 'when scope is generic' do
      it 'returns generic engineering managers for team' do
        expect(group.engineering_managers(:generic)).to eq(['@fake_em'])
      end
    end

    context 'when scope is backend' do
      it 'returns backend engineering managers for team' do
        expect(group.engineering_managers(:backend)).to eq(['@fake_be_em'])
      end
    end

    context 'when scope is frontend' do
      it 'returns frontend engineering managers for team' do
        expect(group.engineering_managers(:frontend)).to eq(['@fake_fe_em'])
      end
    end

    context 'when scope is fullstack' do
      it 'returns frontend engineering managers for team' do
        expect(group.engineering_managers(:fullstack)).to eq(['@fake_fs_em'])
      end
    end

    context 'when several scopes are passed' do
      it 'returns frontend engineering managers for team' do
        expect(group.engineering_managers(:generic, :backend)).to eq(['@fake_be_em', '@fake_em'])
      end
    end

    context 'when scope is not allowed' do
      it 'raises an error' do
        expect { group.engineering_managers(:foo) }.to raise_error(/Unsupported scope `:foo`/)
      end
    end
  end

  describe '#stage' do
    it 'returns the stage' do
      expect(group.stage).to be_a(Hierarchy::Stage)
      expect(group.stage.key).to eq('stage_with_two_groups')
    end
  end

  describe '#default_labeling' do
    context 'when default_labeling config is not present' do
      it 'returns {}' do
        expect(group.default_labeling).to eq({})
      end
    end

    context 'when default_labeling config is present' do
      subject(:group) { described_class.new('group2') }

      it 'returns the default_labeling config' do
        expect(group.default_labeling).to eq({ 'groups' => [1], 'projects' => [2] })
      end
    end
  end

  describe '#default_labeling_exclude_projects' do
    context 'when default_labeling config is not present' do
      it 'returns []' do
        expect(group.default_labeling_exclude_projects).to eq([])
      end
    end

    context 'when default_labeling config is present' do
      context 'when exclude_projects config is not present' do
        subject(:group) { described_class.new('group2') }

        it 'returns []' do
          expect(group.default_labeling_exclude_projects).to eq([])
        end
      end

      context 'when exclude_projects config is present' do
        subject(:group) { described_class.new('group3') }

        it 'returns the default_labeling.exclude_projects config' do
          expect(group.default_labeling_exclude_projects).to eq([3])
        end
      end
    end
  end

  describe '#to_h' do
    it 'returns a hash' do
      expect(group.to_h.keys).to eq(%i[assignees labels mentions stage default_labeling])
    end
  end
end
