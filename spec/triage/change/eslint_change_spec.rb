# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/change/eslint_change'

RSpec.describe Triage::ESLintChange do
  describe '.file_patterns' do
    it 'matches dependency files' do
      files = %w[
        .eslintrc.yml
      ]

      expect(files).to all(match(described_class.file_patterns))
    end
  end
end
