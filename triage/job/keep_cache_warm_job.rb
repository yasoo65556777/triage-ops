# frozen_string_literal: true

require_relative '../triage/job'

module Triage
  class KeepCacheWarmJob < Job
    max_jobs 1

    private

    def execute
      refresh
    ensure
      self.class.perform_in(Triage::GROUP_CACHE_DEFAULT_EXPIRATION - 60)
    end

    def refresh
      Triage::GROUP_CACHE.each do |group, cache_definitions|
        cache_definitions.each do |definition|
          Triage.public_send(definition.fetch(:name), fresh: true)
        end
      end
    end
  end
end
