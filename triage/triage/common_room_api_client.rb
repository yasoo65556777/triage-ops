# frozen_string_literal: true

require 'httparty'

# Wrapper for interacting with the Common Room API
class CommonRoomApiClient
  include HTTParty

  base_uri 'https://api.commonroom.io/community/v1'

  def initialize
    api_key = ENV.fetch('COMMON_ROOM_API_TOKEN', '')
    @headers = {
      'Content-Type' => 'application/json',
      'Authorization' => "Bearer #{api_key}"
    }.freeze
  end

  def add_activity(body)
    response = self.class.post('/source/22150/activity', headers: @headers, body: body.to_json)

    return unless response&.dig('status') != 'ok'

    raise StandardError, "Error sending #{body[:activityType]}-#{body[:id]} to Common Room: #{response}"
  end
end
