# frozen_string_literal: true

require_relative '../../triage'

module Triage
  module PipelineFailure
    # This class is responsible for parsing a job log and extracting data about a detected failure:
    # - failure root cause
    # - failure type
    # - failure trace
    # - failure label
    class JobTraceAnalyzer
      FailureTraceDefinition = Struct.new(:type, :trace_start, :trace_end, :language, :label, keyword_init: true)
      FAILURE_TRACE_DEFINITIONS = [
        FailureTraceDefinition.new(
          type: :rspec,
          trace_start: "Failures:\n",
          trace_end: "\n[TEST PROF INFO]",
          language: :ruby,
          label: '~backend'
        ),
        FailureTraceDefinition.new(
          type: :jest,
          trace_start: "Summary of all failing tests\n",
          trace_end: "\nRan all test suites.",
          language: :javascript,
          label: '~frontend'
        ),
        FailureTraceDefinition.new(
          type: :workhorse,
          trace_start: "make: Entering directory '/builds/gitlab-org/gitlab/workhorse'",
          trace_end: "make: Leaving directory '/builds/gitlab-org/gitlab/workhorse'",
          language: :go,
          label: '~workhorse'
        )
      ].freeze

      TRANSIENT_ROOT_CAUSE_TO_TRACE_MAP =
        {
          failed_to_pull_image: ['job failed: failed to pull image'],
          gitlab_com_overloaded: ['gitlab is currently unable to handle this request due to load'],
          runner_disk_full: ['no space left on device'],
          infrastructure: [
            'the requested url returned error: 5', # any 5XX error code should be transient
            'error: downloading artifacts from coordinator',
            'error: uploading artifacts as "archive" to coordinator',
            '500 Internal Server Error',
            '502 Bad Gateway',
            '503 Service Unavailable',
            'Error: EEXIST: file already exists'
          ],
          job_timeout: [
            'ERROR: Job failed: execution took longer than',
            'Rspec suite is exceeding the 80 minute limit and is forced to exit with error'
          ]
        }.freeze

      FAILURE_TRACE_SIZE_LIMIT = 50000 # Keep it processable by human
      AFTER_SCRIPT_TRACE_START_MARKER = 'Running after_script'

      attr_reader :job_trace

      def initialize(job_trace:)
        @job_trace = job_trace
      end

      def failure_root_cause
        @failure_root_cause ||= TRANSIENT_ROOT_CAUSE_TO_TRACE_MAP.detect do |_root_cause, trace_patterns|
          trace_patterns.any? do |pattern|
            main_trace.downcase.include?(pattern.downcase) && failure_trace_definition.nil?
          end
        end&.first || :default
      end

      def failure_type
        failure_trace_definition&.type
      end

      def failure_trace
        return unless failure_trace_definition

        <<~MARKDOWN.chomp
        ```#{failure_trace_definition&.language}
        #{failure_summary[...FAILURE_TRACE_SIZE_LIMIT]}
        ```
        MARKDOWN
      end

      def failure_label
        failure_trace_definition&.label
      end

      private

      def failure_trace_definition
        return @failure_trace_definition if defined?(@failure_trace_definition)

        @failure_trace_definition = FAILURE_TRACE_DEFINITIONS.find do |failure_trace_definition|
          main_trace.include?(failure_trace_definition.trace_start) &&
            main_trace.include?(failure_trace_definition.trace_end)
        end
      end

      def main_trace
        @main_trace ||= (job_trace.split(AFTER_SCRIPT_TRACE_START_MARKER).first || '')
      end

      def failure_summary
        @failure_summary ||= main_trace
          .split(failure_trace_definition.trace_start)
          .last
          .split(failure_trace_definition.trace_end)
          .first
          .chomp
      end
    end
  end
end
