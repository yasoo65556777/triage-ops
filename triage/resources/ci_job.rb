# frozen_string_literal: true

require_relative '../../lib/constants/labels'
require_relative '../../lib/devops_labels'
require_relative '../../lib/hierarchy/group'
require_relative '../triage'
require_relative '../triage/pipeline_failure/job_trace_analyzer'
require_relative '../triage/pipeline_failure/spec_duration_data'

module Triage
  CiJobBase = Struct.new(:instance, :project_id, :job_id, :name, :web_url, keyword_init: true)

  # This class represents a CI job and provides methods to extract information and act on it:
  # - failure root cause label
  # - potential responsible group labels
  # - markdown link
  # - markdown for test failure summary
  # - markdown for attribution message
  # - markdown for test run time summary
  # - retry the job
  class CiJob < CiJobBase
    SOURCE_CODE_GROUP_LABEL = Hierarchy::Group.new('source_code').label
    CONTAINER_REGISTRY_GROUP_LABEL = Hierarchy::Group.new('container_registry').label
    GITALY_CLUSTER_GROUP_LABEL = Hierarchy::Group.new('gitaly_cluster').label
    INFRASTRUCTURE_GROUP_LABEL = Hierarchy::Group.new('infrastructure').label

    def failure_root_cause_label
      Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS.fetch(job_trace_analyzer.failure_root_cause)
    end

    def potential_responsible_group_labels
      @potential_responsible_group_labels ||=
        case job_trace_analyzer.failure_root_cause
        when :failed_to_pull_image
          [CONTAINER_REGISTRY_GROUP_LABEL]
        when :gitlab_com_overloaded
          [GITALY_CLUSTER_GROUP_LABEL]
        when :infrastructure
          [INFRASTRUCTURE_GROUP_LABEL]
        else
          case job_trace_analyzer.failure_type
          when :workhorse
            [SOURCE_CODE_GROUP_LABEL]
          when :rspec
            failed_rspec_test_metadata.filter_map { |example| example['product_group_label'] }
          when :jest
            [Labels::FRONTEND_LABEL]
          else
            []
          end
        end
    end

    def markdown_link
      "[#{name}](#{web_url})"
    end

    def test_failure_summary_markdown
      [
        "- #{markdown_link}:\n",
        job_trace_analyzer.failure_trace,
        language_label_quick_action,
        related_failure_issues_quick_actions
      ].compact.join("\n")
    end

    def attribution_message_markdown
      case job_trace_analyzer.failure_type
      when :rspec
        failed_rspec_test_metadata.map do |example|
          "- ~\"#{example['product_group_label']}\" ~\"#{example['feature_category_label']}\" #{example['id']}"
        end.join("\n")
      when :workhorse
        "- ~\"#{Hierarchy::Group.new('source_code').label}\" #{name}"
      end
    end

    def rspec_run_time_summary_markdown
      return unless spec_duration_reports.any?

      <<~MARKDOWN.chomp
      - #{markdown_link}:

      <details><summary>Click to expand</summary>

      |file path|expected duration(s)|actual duration(s)|diff %|
      |---------|--------------------|------------------|------|
      #{spec_duration_reports.map(&:markdown_table_row).join("\n")}

      </details>
      MARKDOWN
    end

    def retry
      api_client.job_retry(project_id, job_id)
    end

    private

    def api_client
      @api_client ||= Triage.api_client(instance)
    end

    def trace
      @trace ||= (api_client.job_trace(project_id, job_id) || '')
    end

    def job_trace_analyzer
      @job_trace_analyzer ||= Triage::PipelineFailure::JobTraceAnalyzer.new(job_trace: trace)
    end

    def rspec_results_artifact_file
      @rspec_results_artifact_file ||=
        api_client.download_job_artifact_file(project_id, job_id, "rspec/rspec-retry-#{job_id}.json") ||
        api_client.download_job_artifact_file(project_id, job_id, "rspec/rspec-#{job_id}.json")
    end

    def rspec_results
      @rspec_results ||= begin
        JSON.parse(rspec_results_artifact_file.read)["examples"]
      rescue JSON::ParserError => e
        puts e.message
        []
      end
    end

    def failed_rspec_test_metadata
      return @failed_rspec_test_metadata if defined?(@failed_rspec_test_metadata)

      @failed_rspec_test_metadata ||= rspec_results.select { |example| example["status"] == "failed" }.each do |example|
        labels_map = group_category_labels_for(example['feature_category'])

        if labels_map.present?
          example['product_group_label'] = labels_map['group_label']
          example['feature_category_label'] = labels_map['category_label']
        else
          example['product_group_label'] = DevopsLabels::MISSING_PRODUCT_GROUP_LABEL
          example['feature_category_label'] = DevopsLabels::MISSING_FEATURE_CATEGORY_LABEL
        end
      end
    end

    def failed_tests_related_issues_file
      @failed_job_artirelated_issues_filefact ||=
        api_client.download_job_artifact_file(project_id, job_id, "rspec/#{job_id}-failed-test-issues.json")
    end

    def related_failure_issues
      @related_failure_issues ||= begin
        JSON.parse(failed_tests_related_issues_file.read).fetch(web_url, [])
      rescue JSON::ParserError => e
        puts e.message
        []
      end
    end

    def language_label_quick_action
      return unless job_trace_analyzer.failure_label

      "/label #{job_trace_analyzer.failure_label}"
    end

    def related_failure_issues_quick_actions
      related_failure_issues.map do |issue_url|
        "/relate #{issue_url}"
      end.join("\n").chomp
    end

    def knapsack_expected_durations
      @knapsack_expected_durations ||=
        knapsack_report_from_json('knapsack/node_specs_expected_duration.json')
    end

    def knapsack_actual_durations
      normalized_job_name = name.gsub(%r{[ /]}, '_')
      job_artifact_identifier = "#{normalized_job_name}_#{project_id}"

      @knapsack_actual_durations ||=
        knapsack_report_from_json("knapsack/#{job_artifact_identifier}_report.json")
    end

    def knapsack_report_from_json(file_name)
      report = api_client.download_job_artifact_file(project_id, job_id, file_name).read
      JSON.parse(report)
    rescue StandardError
      {}
    end

    def spec_duration_reports
      return {} unless knapsack_expected_durations.any? && knapsack_actual_durations.any?

      knapsack_actual_durations.keys.map do |spec|
        Triage::PipelineFailure::SpecDurationData.new(
          spec_file: spec,
          expected_duration: knapsack_expected_durations[spec],
          actual_duration: knapsack_actual_durations[spec]
        )
      end
    end

    def group_category_labels_for(feature_category)
      DevopsLabels.group_category_labels_per_category(feature_category)
    end
  end
end
