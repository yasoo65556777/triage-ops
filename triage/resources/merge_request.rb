# frozen_string_literal: true

require_relative 'milestone'

module Triage
  MergeRequest = Struct.new(:iid, :title, :web_url, :labels, :author, :source_branch, :target_branch, :detailed_merge_status, keyword_init: true) do
    def self.build(attrs)
      return unless attrs

      new(attrs.to_h.transform_keys(&:to_sym).slice(*members))
    end

    def self.find(project_id, iid)
      build(Triage.api_client.merge_request(project_id, iid))
    end
  end
end
