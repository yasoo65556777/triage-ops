# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require_relative '../triage/changed_file_list'

module Triage
  class StableE2eCommentOnApproval < Processor
    SKIP_WHEN_CHANGES_ONLY_REGEX = %r{\A(?:docs?|qa|\.gitlab/(issue|merge_request)_templates)/}

    ApprovalMessage = Struct.new(:event) do
      def to_s
        <<~MARKDOWN.strip
          #{header}

          #{body}
        MARKDOWN
      end

      def header
        <<~HEADER.chomp
          :wave: `@#{event.event_actor_username}`, thanks for approving this merge request.
        HEADER
      end

      def body
        <<~BODY.chomp
          This is the first time the merge request has been approved. Please ensure the `e2e:package-and-test-ee` job
          has succeeded. If there is a failure, a Software Engineer in Test (SET) needs to confirm the failures are unrelated to the merge request.
          If there's no SET assigned to this team, ask for assistance on the `#test-platform` Slack channel.
        BODY
      end
    end

    react_to_approvals

    def applicable?
      event.target_branch_is_stable_branch? &&
        event.from_gitlab_org_gitlab? &&
        !changed_file_list.only_change?(SKIP_WHEN_CHANGES_ONLY_REGEX) &&
        unique_comment.no_previous_comment?
    end

    def process
      message = unique_comment.wrap(
        ApprovalMessage.new(event))

      add_discussion(message, append_source_link: false)
    end

    def documentation
      <<~TEXT
        This processor adds a comment on MRs targeting stable branches on canonical after initial approval.
      TEXT
    end

    private

    def changed_file_list
      @changed_file_list ||= Triage::ChangedFileList.new(event.project_id, event.iid)
    end
  end
end
