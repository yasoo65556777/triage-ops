# frozen_string_literal: true

require_relative '../triage/changed_file_list'
require_relative '../triage/react_to_changes/add_as_if_jh'
require_relative '../triage/react_to_changes/comment_changes_for_jihu'
require_relative '../triage/react_to_changes/notify_feature_flag_mr_deployment'
require_relative '../triage/react_to_changes/ping_changes_to_jihu_slack'
require_relative '../triage/processor'

module Triage
  class ReactToChanges < Processor
    react_to \
      'merge_request.open',
      'merge_request.update',
      'merge_request.merge'

    REACTIONS = [
      AddAsIfJH,
      CommentChangesForJiHu,
      NotifyFeatureFlagMrDeployment,
      PingChangesToJiHuSlack
    ].freeze

    def applicable?
      event.from_gitlab_org_gitlab? &&
        event.target_branch_is_main_or_master? &&
        applicable_reactions.any?
    end

    def process
      applicable_reactions.each(&:react)
    end

    def changed_file_list
      @changed_file_list ||= Triage::ChangedFileList.new(event.project_id, event.iid)
    end

    def documentation
      <<~MARKDOWN
        React to merge requests changes. See REACTIONS for more details.
      MARKDOWN
    end

    private

    def applicable_reactions
      @applicable_reactions ||= REACTIONS.filter_map do |reaction_class|
        reaction = reaction_class.new(self)
        reaction.applicable? && reaction
      end
    end
  end
end
